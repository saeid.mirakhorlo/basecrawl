import sys
from crawler import DataCrawler, ImageDownloader, LinkCrawler


def get_page_data():
    raise NotImplementedError


if __name__ == "__main__":
    switch = sys.argv[1]
    if switch == 'find_link':
        crawler = LinkCrawler(
            cities=['paris', 'berlin', 'munich', 'amsterdam'])
        crawler.start(store=True)

    elif switch == 'extract_pages':
        crawler = DataCrawler()
        crawler.start(store=True)
    elif switch =='down_image':
        crawler = ImageDownloader()
        crawler.start()
