
from abc import ABC, abstractmethod
from asyncore import close_all
import json

from mongo import MongoDatabase


class StorageAbstract(ABC):
    @abstractmethod
    def store(self, data, *args):
        pass

    def load(self):
        pass


class FileStorage:

    def store(self, data, file_name, *args):
        file_name = file_name+'-'+data['post_id']
        with open(f'fixtures/adv/{file_name}.json', 'w') as f:
            f.write(json.dumps(data))
        print(f'fixture/adv/{file_name}.json')

    def load(self):
        with open('fixtures/adv/advertisement_links.json', 'r') as f:
            links = json.loads(f.read())
        return links


class MongoStorage:
    def __init__(self) -> None:
        self.mongo = MongoDatabase()

    def store(self, data, collection, *args):
        collection = getattr(self.mongo.database, collection)
        if isinstance(data, list) and len(data) > 1:
            collection.insert_many(data)
        else:
            collection.insert_one(data)

    def load(self, collection_name, filter_data=None):
        collection = self.mongo.database.get_collection(collection_name)
        if filter_data is not None:
            data = collection.find(filter_data)
        else:
            data = collection.find()

        return data

    def updata_flag(self, data):
        """
            due to not to calculate each time each page extraction we set flag to true,

        """
        self.mongo.database.advertisement_links.find_one_and_update(
            {'_id': data['_id']},
            {'$set': {'flag': True}}
        )
