from msilib.schema import Class

from bs4 import BeautifulSoup
from soupsieve import select, select_one


class AdvertisementPageParser:

    def __init__(self) -> None:
        self.soup = None

    @property
    def title(self):
        title_tag = self.soup.find('span', attrs={'id': 'titletextonly'})
        if title_tag:
            return title_tag.text
        return None

    @property
    def price(self):
        price_tag = self.soup.find('span', attrs={'class': 'price'})
        if price_tag:
            return price_tag.text
        return None

    @property
    def body(self):
        body_tag = self.soup.select_one('#postingbody')
        if body_tag:
            return body_tag.text

    @property
    def post_id(self):
        selector = 'body > section > section > section > div.postinginfos > p:nth-child(1)'
        post_id_tag = self.soup.select_one(selector=selector)
        if post_id_tag:
            return post_id_tag.text.replace('post id: ','')
        return None

    @property
    def date_time(self):
        selector = 'body > section > section > section > div.postinginfos > p:nth-child(2) > time'
        date_time_tag = self.soup.select_one(selector)
        if date_time_tag:
            return date_time_tag.attrs['datetime']
        return None

    @property
    def mod_time(self):
        selector = 'body > section > section > section > div.postinginfos > p:nth-child(3) > time'
        mod_time_tag = self.soup.select_one(selector)
        if mod_time_tag:
            return mod_time_tag.attrs['datetime']
        return None
    @property
    def image(self):
        # selector = '#\32 _image_2olCeBEfHCyz_08I0bC > picture > source'
        image_tag = self.soup.select_one('img')
        if image_tag:
            return image_tag.attrs['src']
        return None
    def parse(self, html_data):
        self.soup = BeautifulSoup(html_data, 'html.parser')
        data = dict(
            title=self.title, price=self.price, body=self.body,
             post_id=self.post_id,
             created_time=self.date_time,
            modified_time=self.mod_time,
            image = self.image
        )
    



        # post_id_tag = soup.find()

        return data
        