from abc import ABC, abstractmethod
import requests
from bs4 import BeautifulSoup
import json
from configure import BASE_LINK, STORAGE_TYPE
from page_parser import AdvertisementPageParser
from storage_doc import FileStorage, MongoStorage


class CrawlerBase(ABC):
    def __init__(self):
        self.storage = self.__set_storage()

    @abstractmethod
    def start(self, store=False, file_name=None):
        pass

    @abstractmethod
    def store(self, data, file_name=None):
        pass

    @staticmethod
    def get_page(url):
        try:
            responses = requests.get(url)
        except requests.HTTPError:
            return None

        print(responses.status_code, responses.url)
        return responses.text

    @staticmethod
    def __set_storage():
        if STORAGE_TYPE == 'mongo':
            return MongoStorage()
        return FileStorage()


class LinkCrawler(CrawlerBase):
    def __init__(self, cities=None, link=BASE_LINK) -> None:
        self.cities = cities
        self.link = link
        super().__init__()

    def find_link(self, html_text):
        soup = BeautifulSoup(html_text, 'html.parser')
        # content = soup.find('div', attrs={'class':'content'})
        # adv_lsit = soup.find('li', attrs={'class':'result-now'})
        return soup.find_all('a', attrs={'class': 'hdrlnk'})

    def start_crawl_city(self, url):
        start = 0
        crawl = True
        anchor_links = []
        while crawl:
            html_response = self.get_page(url+str(start))
            new_links = self.find_link(html_response)
            anchor_links.extend(new_links)
            start += 120
            crawl = bool(len(new_links))
        return anchor_links

    def start(self, store=False):
        adv_links = []
        for city in self.cities:
            links = self.start_crawl_city(self.link.format(city))
            print(f"city: {city}, total:", len(links))
            adv_links.extend(
                [{'url': link.get('href'), 'flag': False} for link in links])
            # adv_links.extend(links)
        if store:
            self.store(adv_links, 'advertisement_links')
        return adv_links

    def store(self, data, collection, *args):
        self.storage.store(data, collection)
        # with open('fixtures/data.json', 'w') as f:
        #     f.write(json.dumps(data))


class DataCrawler(CrawlerBase):

    def __init__(self) -> None:
        super().__init__()
        self.links = self.__load_links()
        self.parser = AdvertisementPageParser()

    def __load_links(self):
        return self.storage.load('advertisement_links', filter_data={'flag': False})

    def start(self, store=False):
        for link in self.links:
            response = self.get_page(link['url'])
            data = self.parser.parse(response)
            if store:
                self.store(data, data.get('post_id', 'sample'))
            # print(data)
            self.storage.updata_flag(link)

    def store(self, data, file_name):
        self.storage.store(data, 'advertisemanet_data')


class ImageDownloader(CrawlerBase):
    def __init__(self):
        super().__init__()
        self.advertisements = self.__load_advertisement()

    def __load_advertisement(self):
        return self.storage.load('advertisemanet_data')

    @staticmethod
    def get_page(url):
        try:
            responses = requests.get(url, stream=True)
        except requests.HTTPError:
            return None

        print(responses.status_code, responses.url)
        return responses

    def start(self, store=True):
        for advertisement in self.advertisements:
            counter = 0
            image =advertisement.get("image")
            response = self.get_page(image)
            if store:
                self.store(response, advertisement['post_id'], counter)
            counter += 1



    def store(self,data,adv_id, img_number):
        filename = f'{adv_id}-{img_number}'
        return self.save_to_disk(data , filename)


    def save_to_disk(self, response,filename,):
        with open(f'fixtures/images/{filename}.jpeg', 'ab') as f:
            f.write(response.content)
            for _ in response.iter_content():
                f.write(response.content)

        print(filename)
        return filename
